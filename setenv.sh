#!/bin/sh

#This part discovers what the base dir is
DIR=`pwd`
cd `dirname $0`
SCRIPTPATH=`pwd`
cd "$DIR"
cd "$SCRIPTPATH/../.."
THEINSTALL_DIR=`pwd`
unset DIR
INSTALL_DIR=$THEINSTALL_DIR
#Done getting base directory

unset CLASSPATH
#IQ_OPTS=-Dsample=option -Dsample1=option
#export IQ_OPTS

"$INSTALL_DIR/bin/iqcmd.sh" InfoCenter default development indexing
