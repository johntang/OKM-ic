FROM okm:base
LABEL maintainer="johnt@softclouds.com"
WORKDIR /opt
USER okm
RUN wget https://gitlab.com/johntang/OKM-ic/raw/master/okm-ic.tar.gz
RUN mv okm-ic.tar.gz okm.tar.gz && \
    tar xfvz okm.tar.gz && \
    rm okm.tar.gz && \
    rm -rf /opt/okm/im/instances/InfoManager/appserverim/webapps/examples && \
    rm -rf /opt/okm/im/instances/InfoManager/appserverim/webapps/docs
WORKDIR /opt/okm/im/InfoManager/config/IMADMIN
RUN rm application.properties && wget https://gitlab.com/johntang/OKM-ic/raw/master/application.properties
WORKDIR /opt/okm/im/instances/InfoManager
